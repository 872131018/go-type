package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"
)

func getQuote() Quote {
	files, err := ioutil.ReadDir("../lib/genres")
	if err != nil {
		log.Fatal(err)
	}

	rand.Seed(time.Now().Unix())
	category := files[rand.Intn(len(files))]

	jsonFile, err := os.Open("../lib/genres/" + category.Name())
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()

	bytes, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatal(err)
	}

	var quotes []Quote
	json.Unmarshal(bytes, &quotes)
	if err != nil {
		log.Fatal(err)
	}

	return quotes[rand.Intn(len(quotes))]
}
