package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"time"

	. "github.com/logrusorgru/aurora"
	termbox "github.com/nsf/termbox-go"
)

type Quotes struct {
	Quote []Quote `json:"quotes"`
}

type Quote struct {
	Quote  string `json:"quote"`
	Author string `json:"author"`
	Genre  string `json:"genre"`
}

type State struct {
	Quote    Quote  `json:"quote"`
	Index    int    `json:"index"`
	Progress string `json:"progress"`
	Entered  string `json:"entered"`
	Correct  int    `json:"correct"`
	Time     int    `json:"time"`
}

func main() {
	state := State{
		Quote:    getQuote(),
		Index:    0,
		Progress: "",
		Correct:  0,
		Time:     0,
	}

	err := termbox.Init()
	if err != nil {
		log.Fatal(err)
	}
	defer termbox.Close()

	// Setup simple timer
	ticker := time.NewTicker(1 * time.Second)
	go func() {
		for range ticker.C {
			state.Time++
			display(state)
		}
	}()

	// Display initial screen
	display(state)

	// Main loop
	for {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			switch ev.Key {
			case termbox.KeyEsc:
				log.Fatal("Quitting application...")
			case termbox.KeyBackspace:
			case termbox.KeyBackspace2:
				state.Entered = state.Entered[:len(state.Entered)-1]
				state.Index--
			case termbox.KeySpace:
				state.Entered = state.Entered + " "
				if " " == string(state.Quote.Quote[state.Index]) {
					state.Correct++
				}
				state.Index++
			case termbox.KeyEnter:
				log.Fatal("hit enter")
				ticker.Stop()
			default:
				state.Entered = state.Entered + string(ev.Ch)
				if string(ev.Ch) == string(state.Quote.Quote[state.Index]) {
					state.Correct++
				}
				state.Index++
			}
			display(state)
		case termbox.EventError:
			log.Fatal("Error has occured.")
		}
	}
}

func display(state State) {
	// Clear screen and display new view
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()

	// Update progress coloring
	state = updateProgress(state)
	correctPercentage := float64(state.Correct) / float64(len(state.Quote.Quote)) * 100

	fmt.Printf("Progress: %v\n", state.Progress)
	fmt.Printf("Author - %v\n", state.Quote.Author)
	fmt.Println()
	fmt.Printf("Time - %v\n", state.Time)
	fmt.Println()
	fmt.Printf("Entered: %v\n", state.Entered)
	fmt.Println()
	fmt.Printf("Correct percentage: %v\n", fmt.Sprintf("%.1f", correctPercentage))
}

func updateProgress(state State) State {
	state.Progress = ""
	for i, c := range state.Quote.Quote {
		char := string(c)
		str := ""
		if state.Index > i {
			if char != string(state.Entered[i]) {
				str = Sprintf("%s", BgRed(char).Black())

			} else {
				str = Sprintf("%s", BgGreen(char).Black())
			}
		} else {
			str = Sprintf("%s", Gray(1-1, char).BgGray(24-1))
		}
		state.Progress = state.Progress + str
		i++
	}
	return state
}
